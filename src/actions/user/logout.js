import axios from 'axios';
import API from '../../constants/api';
import { USER_RESET } from '../types';

export default function logout(authToken = null) {
  return dispatch => new Promise(async (resolve, reject) => {
    if ( authToken == null || authToken === undefined || !authToken ) {
      dispatch({ type: USER_RESET });
      setTimeout(resolve, 1000);
    } else {
      await axios.delete(API.USER_LOGOUT, { 
        headers: { 'x-auth': authToken } 
      })
      .then( resolve => {
        dispatch({ type: USER_RESET });
        setTimeout(resolve, 1000);
      })
      .catch(reject).catch((err) => { throw err.message; });
    }
  })
}