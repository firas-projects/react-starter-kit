import  {USER_DETAILS_UPDATE} from '../types';

export default function getUserData(user) {
  return dispatch => new Promise((resolve, reject) => {
    return getUserDataDispatch( dispatch, user ); 
  });

  function getUserDataDispatch( dispatch, data = null ) {
    return dispatch({
      type: USER_DETAILS_UPDATE,
      data,
    });
  }

}