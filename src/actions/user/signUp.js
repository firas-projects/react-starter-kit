import axios from 'axios';
import { errorMessages } from '../../constants/messages';
import API from '../../constants/api';

// import {
//   USER_DETAILS_UPDATE,
//   USER_LOGIN,
//   USER_RESET
// } from '../actions/types';

/**
  * Sign Up to MongoDB
  */
export default function signUp(formData) {
  const {
    email,
    password,
    password2,
    firstName,
    lastName,
  } = formData;


  return dispatch => new Promise(async (resolve, reject) => {
    // Validation rules
    if (!firstName) return reject({ message: errorMessages.missingFirstName });
    if (!lastName) return reject({ message: errorMessages.missingLastName });
    if (!email) return reject({ message: errorMessages.missingEmail });
    if (!password) return reject({ message: errorMessages.missingPassword });
    if (!password2) return reject({ message: errorMessages.missingPassword });
    if (password !== password2) return reject({ message: errorMessages.passwordsDontMatch });

    //await statusMessage(dispatch, 'loading', true);
    const userData = { firstName, lastName, email, password }
    await axios.post(API.USER, userData)
    .then((response) => {
      //statusMessage(dispatch, 'loading', false);
      return resolve(dispatch({
        //type: USER_CREATE
      }));
    })
    .catch( reject );
  });
}