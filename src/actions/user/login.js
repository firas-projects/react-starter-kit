import axios from 'axios';
import { errorMessages } from '../../constants/messages';
import API from '../../constants/api';
import { USER_LOGIN } from '../types';

export default function login(formData) {
  const { email,password } = formData;

  return dispatch => new Promise(async (resolve, reject) => { 
    // Validation checks
    if (!email) return reject({ data: { message: errorMessages.missingEmail } });
    if (!password) return reject({ data: {message: errorMessages.missingPassword } });

    await axios.post(API.USER_LOGIN, { email, password })
    .then((response) => {
      const userData = response.data;
      userData.authToken = response.headers['x-auth'];
      return resolve(dispatch({
        type: USER_LOGIN,
        data: userData,
      }));
    })
    .catch(reject);
  });
}