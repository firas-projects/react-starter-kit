import axios from 'axios';
import { errorMessages } from '../../constants/messages';
import API from '../../constants/api';
import { USER_FORGOT } from '../types';

/**
* Reset Password
*/
export default function resetPassword(formData) {
  const { email } = formData;

  return dispatch => new Promise(async (resolve, reject) => {
    // Validation checks
    if (!email) return reject({ message: errorMessages.missingEmail });

    await axios.post( API.USER_FORGOT, { email })
    .then( resolve => {
      dispatch({ type: USER_FORGOT });
    })
    .catch(reject);
  });
}
