export const USER_CREATE = 'USER_CREATE';
//export const USER_ERROR = 'USER_ERROR';
export const USER_DETAILS_UPDATE = 'USER_DETAILS_UPDATE';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_FORGOT = 'USER_FORGOT';
export const USER_RESET = 'USER_RESET';