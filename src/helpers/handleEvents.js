import { useState } from 'react';

function useHandleEvents( startingValue = null ) {
	const [state, setState] = useState( startingValue );

	function handleChange(e) {
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
		setState({ ...state, [e.target.name] : value });
	}

	// const handleAction = (action) => {
	// 	return action( state )
	// 	.catch(e => console.log(`Error:`, e.message));
	// }

	// const handleSubmit = (action) => {
	// 	return handleAction(action);
	// }

	// const handleError = (e) => {
	// 	// error
	// }

	return { 
		state, 
		setState,
		handleChange,
		// handleAction,
		// handleSubmit
	};
}



export {
	useHandleEvents
}