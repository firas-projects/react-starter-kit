import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { resetPassword } from '../actions/user';
import { useHandleEvents } from '../helpers/handleEvents';

function ForgotPassword(props) {
  const { member, Layout, resetPassword } = props;
  const initialState = {
    error: null,
    success: null,
    loading: false,
  }
  const { state, setState } = useHandleEvents(initialState);
  const { error, loading, success } = state;

  const handleForgotPassword = (data) => {
    setState({ loading: true });
    return resetPassword(data)
      .then(() => setState({
        loading: false,
        success: 'Success - Reset link emailed',
        error: null,
      })).catch((err) => {
        setState({
          loading: false,
          success: null,
          error: err,
        });
        throw err; // To prevent transition back
      });
  }


  return (
    <Layout
      error={error}
      member={member}
      loading={loading}
      success={success}
      onForgotPassword={handleForgotPassword}
    />
  );
}

ForgotPassword.propTypes = {
  Layout: PropTypes.func.isRequired,
  member: PropTypes.shape({}).isRequired,
  resetPassword: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  member: state.member || {},
});

const mapDispatchToProps = {
  resetPassword,
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
