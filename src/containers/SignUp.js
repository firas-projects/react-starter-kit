import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { signUp } from '../actions/user';
import { useHandleEvents } from '../helpers/handleEvents';

function SignUp(props) {

  const { member, Layout, signUp } = props;

  const initialState = {
    error: null,
    success: null,
    loading: false,
  }
  const { state, setState } = useHandleEvents(initialState);
  const { error, loading, success } = state;

  const handleSignUp = (data) => {
    setState({ ...state, loading: true });

    return signUp(data)
      .then(() => setState({
        loading: false,
        success: 'Success - You are now a member',
        error: null,
      })).catch((err) => {
        setState({
          loading: false,
          success: null,
          error: err,
        });
        throw err; // To prevent transition back
      });
  }

  return (
    <Layout
      error={error}
      member={member}
      loading={loading}
      success={success}
      onSignUp={handleSignUp}
    />
  );




}

SignUp.propTypes = {
  Layout: PropTypes.func.isRequired,
  member: PropTypes.shape({}).isRequired,
  signUp: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  member: state.member || {},
});

const mapDispatchToProps = {
  signUp,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);