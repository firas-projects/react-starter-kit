import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { login } from '../actions/user';
import { useHandleEvents } from '../helpers/handleEvents';

function Login (props) {

  const { login, member, Layout } = props;

  const initialState = {
    error: null,
    success: null,
    loading: false,
  }
  const { state, setState } = useHandleEvents(initialState);
  const { error, loading, success } = state;

  const handleLogin = (data) => {
    setState( { ...state, loading: true } );
    
    return login(data)
      .then(() => setState({
        loading: false,
        success: 'Success - Logged in',
        error: null,
      })).catch((err) => {
        setState({
          loading: false,
          success: null,
          error: err,
        });
        throw err; // To prevent transition back
      });
  }

  return (
    <Layout
      error={error}
      member={member}
      loading={loading}
      success={success}
      onLogin={handleLogin}
    />
  );
} 

Login.propTypes = {
  Layout: PropTypes.func.isRequired,
  member: PropTypes.shape({}).isRequired,
  onLogin: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  member: state.member || {},
});

const mapDispatchToProps = {
  login
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
