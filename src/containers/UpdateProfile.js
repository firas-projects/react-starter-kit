import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updateProfile } from '../actions/user';
import { useHandleEvents } from '../helpers/handleEvents';

function UpdateProfile(props) {

  const { updateProfile, user, Layout } = props;

  const initialState = {
    error: null,
    success: null,
    loading: false,
  }
  const { state, setState } = useHandleEvents(initialState);
  const { error, loading, success } = state;

  const handleUpdateProfile = (data) => {
    
    setState({ ...state, loading: true });

    return updateProfile(data)
      .then(() => setState({
        loading: false,
        success: 'Success - Updated',
        error: null,
      })).catch(err => setState({
        loading: false,
        success: null,
        error: err,
      }));
  }

  return (
    <Layout
      error={error}
      user={user}
      loading={loading}
      success={success}
      onUpdateProfile={handleUpdateProfile}
    />
  );

}

UpdateProfile.propTypes = {
  Layout: PropTypes.func.isRequired,
  user: PropTypes.shape({}).isRequired,
  updateProfile: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  user: state.user || {},
});

const mapDispatchToProps = {
  updateProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);
