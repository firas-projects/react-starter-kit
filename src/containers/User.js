import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { logout, getUserData } from '../actions/user';
import { useHandleEvents } from '../helpers/handleEvents';

function User(props) {

  const { Layout, user, logout, getUserData } = props;

  const initialState = { error: null, loading: false }
  const { state, setState } = useHandleEvents(initialState);
  const { error, loading} = state;

  useEffect( () => { 
    handleGetUserData()
  })

  const handleGetUserData = useCallback(() => {
    setState({ ...state, loading: true });
    return getUserData()
      .then(() => setState({
        loading: false,
        error: null,
      })).catch(err => setState({
        loading: false,
        error: err,
      }));
  },[state, setState, getUserData]);


  return (
    <Layout
      error={error}
      loading={loading}
      user={user}
      logout={logout}
      reFetch={ () => handleGetUserData() }
    />
  );
}


User.propTypes = {
  Layout: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  getUserData: PropTypes.func.isRequired,
  user: PropTypes.shape({}).isRequired,
}

const mapStateToProps = state => ({
  user: state.user || {},
});

const mapDispatchToProps = {
  logout,
  getUserData,
};

export default connect(mapStateToProps, mapDispatchToProps)(User);