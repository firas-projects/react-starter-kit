import React from 'react';
import { Scene, Tabs, Stack } from 'react-native-router-flux';
import { Icon } from 'native-base';

import DefaultProps from '../constants/navigation';
import AppConfig from '../../constants/config';

// import SignUpContainer from '../../containers/SignUp';
// import SignUpComponent from '../components/User/SignUp';

// import LoginContainer from '../../containers/Login';
// import LoginComponent from '../components/User/Login';

// import ForgotPasswordContainer from '../../containers/ForgotPassword';
// import ForgotPasswordComponent from '../components/User/ForgotPassword';

// import UpdateProfileContainer from '../../containers/UpdateProfile';
// import UpdateProfileComponent from '../components/User/UpdateProfile';

// import MemberContainer from '../../containers/Member';
// import ProfileComponent from '../components/User/Profile';

import HomeComponent from '../components/Home';
//import AboutComponent from '../components/About';

const Index = (
  <Stack hideNavBar>
    <Scene hideNavBar>
      <Tabs
        key="tabbar"
        swipeEnabled
        type="replace"
        showLabel={false}
        {...DefaultProps.tabProps}
      >
        <Stack
          key="home"
          title={AppConfig.appName.toUpperCase()}
          icon={() => <Icon name="planet" {...DefaultProps.icons} />}
          {...DefaultProps.navbarProps}
        >
          <Scene key="home" component={HomeComponent} />
        </Stack>


        <Stack
          key="profile"
          title="PROFILE"
          icon={() => <Icon name="contact" {...DefaultProps.icons} />}
          {...DefaultProps.navbarProps}
        >
          
        </Stack>

      </Tabs>
    </Scene>

  </Stack>
);

export default Index;