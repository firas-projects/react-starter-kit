import {
  USER_DETAILS_UPDATE,
  USER_LOGIN,
  USER_RESET
} from '../actions/types';

const initialState = {}

export default function userReducer(state = initialState, action) {
  
  switch (action.type) {
    
    case USER_LOGIN: {
      if (action.data) {
        return {
          ...state,
          authToken: action.data.authToken,
          id: action.data._id,
          email: action.data.email,
          //emailVerified: action.data.emailVerified,
        };
      }
      return initialState;
    }

    case USER_DETAILS_UPDATE: {
      if (action.data) {
        return {
          ...state,
          firstName: action.data.firstName,
          lastName: action.data.lastName,
          signedUp: action.data.signedUp,
          role: action.data.role,
        };
      }
      return initialState;
    }

    case USER_RESET: {
      return initialState;
    }
    default:
      return state;
  }

}