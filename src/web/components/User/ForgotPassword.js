import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';

import { useHandleEvents } from '../../../helpers/handleEvents';

function ForgotPassword(props) {
  const { onForgotPassword, history, loading, error, success, user } = props;
  const initialState = {
    email: (user && user.email) ? user.email : ''
  }
  const { state, handleChange } = useHandleEvents(initialState);
  const { email } = state;

  const handleSubmit = (e) => {
    e.preventDefault();
    return onForgotPassword(state)
    .then(() => setTimeout(() => history.push('/login'), 1000))
    .catch(() => {});
  }

  return (
    <div>
      <Row>
        <Col lg={{ size: 6, offset: 3 }}>
          <Card>
            <CardHeader>
              Forgot Password
            </CardHeader>
            <CardBody>
              {!!error && <Alert color="danger">{error}</Alert>}
              {!!success && <Alert color="success">{success}</Alert>}
              <Form onSubmit={handleSubmit}>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Input
                    type="email"
                    name="email"
                    id="email"
                    placeholder="john@doe.corp"
                    value={email}
                    disabled={loading}
                    onChange={handleChange}
                  />
                </FormGroup>
                <Button color="primary" disabled={loading}>
                  {loading ? 'Loading' : 'Reset Password'}
                </Button>
              </Form>

              <hr />

              <Row>
                <Col sm="6">
                  Need an account?
                  {' '}
                  <Link to="/sign-up">Sign Up</Link>
                </Col>
                <Col sm="6" className="text-right">
                  <Link to="/login">Log in</Link>
                  {' '}
                  to your account.
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

ForgotPassword.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
  }),
  error: PropTypes.string,
  success: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
}

ForgotPassword.defaultProps = {
  error: null,
  success: null,
  user: {},
}

export default withRouter(ForgotPassword);