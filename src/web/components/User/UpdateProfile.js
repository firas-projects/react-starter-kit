import React from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';

import { useHandleEvents } from '../../../helpers/handleEvents';

function UpdateProfile(props) {

  const { loading, success, error, onUpdateProfile } = props;
  const initialState = {
    firstName: props.user.firstName || '',
    lastName: props.user.lastName || '',
    email: props.user.email || '',
    password: '',
    password2: '',
    changeEmail: false,
    changePassword: false,
  }
  const { state, handleChange } = useHandleEvents(initialState);

  const {
    firstName, lastName, changeEmail, email, changePassword, password, password2,
  } = state;

  const handleSubmit = (e) => {
    e.preventDefault();
    onUpdateProfile(state).catch(() => {});
  }

  return (
    <div>
      <Row>
        <Col lg={{ size: 6, offset: 3 }}>
          <Card>
            <CardHeader>Update Profile</CardHeader>
            <CardBody>
              {!!error && <Alert color="danger">{error}</Alert>}
              {!!success && <Alert color="success">{success}</Alert>}

              <Form onSubmit={handleSubmit}>
                <FormGroup>
                  <Label for="firstName">First Name</Label>
                  <Input
                    type="text"
                    name="firstName"
                    id="firstName"
                    placeholder="John"
                    disabled={loading}
                    value={firstName}
                    onChange={handleChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="lastName">Last Name</Label>
                  <Input
                    type="text"
                    name="lastName"
                    id="lastName"
                    placeholder="Doe"
                    disabled={loading}
                    value={lastName}
                    onChange={handleChange}
                  />
                </FormGroup>

                <FormGroup check style={{ marginTop: 20 }}>
                  <Label check>
                    <Input
                      type="checkbox"
                      name="changeEmail"
                      checked={changeEmail}
                      onChange={handleChange}
                    />
                    {' '}
                    Change my email
                  </Label>
                </FormGroup>
                {changeEmail && (
                  <FormGroup>
                    <Label for="email">Email</Label>
                    <Input
                      type="email"
                      name="email"
                      id="email"
                      placeholder="email@example.com"
                      disabled={loading}
                      value={email}
                      onChange={handleChange}
                    />
                  </FormGroup>
                )}

                <FormGroup check style={{ marginTop: 20 }}>
                  <Label check>
                    <Input
                      type="checkbox"
                      name="changePassword"
                      checked={changePassword}
                      onChange={handleChange}
                    />
                    {' '}
                    Change my password
                  </Label>
                </FormGroup>
                {changePassword && (
                  <div>
                    <FormGroup>
                      <Label for="password">Password</Label>
                      <Input
                        type="password"
                        name="password"
                        id="password"
                        placeholder="••••••••"
                        disabled={loading}
                        value={password}
                        onChange={handleChange}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label for="password2">Confirm Password</Label>
                      <Input
                        type="password"
                        name="password2"
                        id="password2"
                        placeholder="••••••••"
                        disabled={loading}
                        value={password2}
                        onChange={handleChange}
                      />
                    </FormGroup>
                  </div>
                )}
                <Button style={{ marginTop: 20 }} color="primary" disabled={loading}>
                  {loading ? 'Loading' : 'Update Profile'}
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}


UpdateProfile.propTypes = {
  error: PropTypes.string,
  success: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    email: PropTypes.string,
  }).isRequired,
}

UpdateProfile.defaultProps = {
  error: null,
  success: null,
}

export default UpdateProfile;
