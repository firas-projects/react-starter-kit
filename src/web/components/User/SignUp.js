import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';

import { useHandleEvents } from '../../../helpers/handleEvents';

function SignUp(props) {
  const { onFormSubmit, history, loading, error, success } = props;
  const initialState = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    password2: '',
  }
  const { state, handleChange } = useHandleEvents(initialState);
  const { firstName, lastName, email, password, password2 } = state;

  const handleSubmit = (e) => {
    e.preventDefault();
    onFormSubmit(state)
      .then(() => setTimeout(() => history.push('/login'), 1000))
      .catch(() => {});
  }

  return (
    <div>
      <Row>
        <Col lg={{ size: 6, offset: 3 }}>
          <Card>
            <CardHeader>Sign Up</CardHeader>
            <CardBody>
              {!!error && <Alert color="danger">{error}</Alert>}
              {!!success && <Alert color="success">{success}</Alert>}

              <Form onSubmit={handleSubmit}>
                <FormGroup>
                  <Label for="firstName">First Name</Label>
                  <Input
                    type="text"
                    name="firstName"
                    id="firstName"
                    placeholder="John"
                    disabled={loading}
                    value={firstName}
                    onChange={handleChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="lastName">Last Name</Label>
                  <Input
                    type="text"
                    name="lastName"
                    id="lastName"
                    placeholder="Doe"
                    disabled={loading}
                    value={lastName}
                    onChange={handleChange}
                  />
                </FormGroup>

                <FormGroup style={{ marginTop: 40 }}>
                  <Label for="email">Email</Label>
                  <Input
                    type="email"
                    name="email"
                    id="email"
                    placeholder="john@doe.corp"
                    disabled={loading}
                    value={email}
                    onChange={handleChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="password">Password</Label>
                  <Input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="••••••••"
                    disabled={loading}
                    value={password}
                    onChange={handleChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="password2">Confirm Password</Label>
                  <Input
                    type="password"
                    name="password2"
                    id="password2"
                    placeholder="••••••••"
                    disabled={loading}
                    value={password2}
                    onChange={handleChange}
                  />
                </FormGroup>
                <Button color="primary" disabled={loading}>
                  {loading ? 'Loading' : 'Sign Up'}
                </Button>
              </Form>

              <hr />

              <Row>
                <Col sm="12">
                  Already have an account?
                  {' '}
                  <Link to="/login">Log in here</Link>.
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

SignUp.propTypes = {
  error: PropTypes.string,
  success: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
}

SignUp.defaultProps = {
  error: null,
  success: null,
}

export default withRouter(SignUp);