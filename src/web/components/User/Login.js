import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';

import { useHandleEvents } from '../../../helpers/handleEvents';

function Login(props) {
  const { onLogin, history, loading, success, error, user } = props;
  const initialState = {
    email: (user && user.email) ? user.email : '',
    password: '',
  }
  const { state, handleChange } = useHandleEvents(initialState);
  const { email, password } = state;

  const handleSubmit = (e) => {
    e.preventDefault();
    return onLogin(state)
    .then(() => setTimeout(() => history.push('/'), 1000))
    .catch(() => {});
  }

  return (
    <div>
      <Row>
        <Col lg={{ size: 6, offset: 3 }}>
          <Card>
            <CardHeader>Login</CardHeader>
            <CardBody>
              {!!success && <Alert color="success">{success}</Alert>}
              {!!error && <Alert color="danger">{error}</Alert>}

              <Form onSubmit={handleSubmit}>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Input
                    type="email"
                    name="email"
                    id="email"
                    placeholder="john@doe.corp"
                    disabled={loading}
                    value={email}
                    onChange={handleChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="password">Password</Label>
                  <Input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="••••••••"
                    disabled={loading}
                    value={password}
                    onChange={handleChange}
                  />
                </FormGroup>
                <Button color="primary" disabled={loading}>
                  {loading ? 'Loading' : 'Login' }
                </Button>
              </Form>

              <hr />

              <Row>
                <Col sm="6">
                  Need an account?
                  {' '}
                  <Link to="/sign-up">Sign Up</Link>
                </Col>
                <Col sm="6" className="text-right">
                  <Link to="/forgot-password">Forgot Password?</Link>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
  
}




Login.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
  }),
  error: PropTypes.string,
  success: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
}

Login.defaultProps = {
  error: null,
  success: null,
  user: {},
}


export default withRouter(Login);
