import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Nav,
  Navbar,
  Collapse,
  DropdownMenu,
  DropdownItem,
  NavbarToggler,
  DropdownToggle,
  UncontrolledDropdown,
} from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import Config from '../../../constants/config';
import { SidebarNavItems } from './Sidebar';

function Header(props) {

  const { logout, history, user } = props;
  const [ isOpen, setIsOpen ] = useState(false);

  const loggedIn = !!(user && user.email);

  const onLogout = () => logout().then(() => history.push('/login'));
  const toggleDropDown = () => setIsOpen(!isOpen);

  return (
    <header>
      <Navbar dark color="primary" expand="sm" className="fixed-top">
        <Link to="/" className="navbar-brand" style={{ color: '#FFF' }}>

          {Config.appName}
        </Link>
        <NavbarToggler onClick={toggleDropDown} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <div className="d-block d-sm-none">
              {SidebarNavItems()}
            </div>
            <UncontrolledDropdown nav>
              <DropdownToggle nav caret>
                {loggedIn ? `Hi, ${user.firstName}` : 'My Account'}
              </DropdownToggle>
              <DropdownMenu>
                {!loggedIn
                  && (
                  <div>
                    <DropdownItem tag={Link} to="/login">
                      Login
                    </DropdownItem>
                    <DropdownItem tag={Link} to="/sign-up">
                      Sign Up
                    </DropdownItem>
                  </div>
                  )
                }
                {loggedIn
                  && (
                  <div>
                    <DropdownItem tag={Link} to="/update-profile">
                      Update Profile
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem tag="button" onClick={onLogout}>
                      Logout
                    </DropdownItem>
                  </div>
                  )
                }
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar>
    </header>
  );
}

Header.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string,
    email: PropTypes.string,
  }),
  logout: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
}

Header.defaultProps = {
  user: {},
}

export default withRouter(Header);