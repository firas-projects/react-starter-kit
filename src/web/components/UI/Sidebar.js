/* global window */
import React from 'react';
import { Col, Nav, NavItem } from 'reactstrap';
import { Link } from 'react-router-dom';

const SidebarNavItems = () => {
  const {pathname} = window.location;
  return (
    <div>
      <NavItem>
        <Link className={`nav-link ${pathname === '/' && 'active'}`} to="/">
          <i className="icon-home" />
          {' '}
          <span>Home</span>
        </Link>
      </NavItem>
      <NavItem>
        <Link className={`nav-link ${pathname.startsWith('/another-page') && 'active'}`} to="/another-page">
          <i className="icon-notebook" />
          {' '}
          <span>Coming Soon</span>
        </Link>
      </NavItem>
    </div>
  )
};

const Sidebar = () => (
  <div>
    <Col sm="3" md="2" className="d-none d-sm-block sidebar">
      <Nav vertical>
        {SidebarNavItems()}
      </Nav>
    </Col>
  </div>
);

export { Sidebar, SidebarNavItems };