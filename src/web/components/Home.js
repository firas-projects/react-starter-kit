import React from 'react';
import { Row, Jumbotron } from 'reactstrap';

const Home = () => (
  <div>
    <Row>
      <Jumbotron className="bg-primary text-white">
        <h1>
          Web & Mobile App Starter Kit
        </h1>
      
        <p>
          An app starter kit to create a web app + mobile app, where you don't need to write and maintain two different code bases. 
          The project shares the 'business logic' and allows flexibility in View components to ensure your project looks and feels
          native in each platform.
        </p>
      </Jumbotron>
    </Row>
  </div>
);

export default Home;
