/* global document */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { PersistGate } from 'redux-persist/es/integration/react';

import configureStore from '../store';
import * as serviceWorker from './serviceWorker';
import Routes from './routes';

// Components
import Loading from './components/UI/Loading';

// Load css
import './styles/style.scss';

const { persistor, store } = configureStore();
// persistor.purge(); // Debug to clear persist

const Root = () => (
	<Provider store={store}>
		<PersistGate loading={<Loading />} persistor={persistor}>
			<Router>
				<Routes />
			</Router>
		</PersistGate>
  </Provider>
);

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();








// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import * as serviceWorker from './serviceWorker';

// ReactDOM.render(<App />, document.getElementById('root'));

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();




// import React from 'react';
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   	return (
// 		<div className="App">
// 			<header className="App-header">
// 				<img src={logo} className="App-logo" alt="logo" />
// 				<p>
// 				Edit <code>src/App.js</code> and save to reload.
// 				</p>
// 				<a
// 				className="App-link"
// 				href="https://reactjs.org"
// 				target="_blank"
// 				rel="noopener noreferrer"
// 				>
// 				Learn React
// 				</a>
// 			</header>
// 		</div>
// 	);
// }

// export default App;
