export default {
  apiKey: 'XXXXXXXXXXXXXXXXXXXXXXXX',
  authDomain: 'XXXXXXXXXXXXXXX.firebaseapp.com',
  databaseURL: 'https://XXXXXXXXXXXXXXX.firebaseio.com',
  projectId: 'XXXXXXXXXXXXXXXXXX',
  storageBucket: 'XXXXXXXXXXXXXXXXXXXXX.appspot.com',
  messagingSenderId: 'XXXXXXXXXXXXXXX',
};