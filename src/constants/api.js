const devMode = (process.env.NODE_ENV !== 'development');

const baseUrl = (devMode) ? 'http://localhost:3000/api/v1' : 'http://localhost:3000/api/v1';

export default {
  HOME: `${baseUrl}`,
  USER: `${baseUrl}/user`,
  USER_LOGIN: `${baseUrl}/user/login`,
	USER_LOGOUT: `${baseUrl}/user/logout`,
	USER_FORGOT: `${baseUrl}/user/forgotpassword`,
	USER_CONFIRM: `${baseUrl}/user/confirm`,
}