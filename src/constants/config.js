const devMode = (process.env.NODE_ENV !== 'development');

export default {
  // App Details
  appName: 'App Starter Kit',

  // Build Configuration - eg. Debug or Release?
  DEV: devMode,

  // Google Analytics - uses a 'dev' account while we're testing
  //gaTrackingId: (devMode) ? 'UA-XXXXXXXX-X' : 'UA-XXXXXXXX-X',
};