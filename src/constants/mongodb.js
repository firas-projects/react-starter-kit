const devMode = (process.env.NODE_ENV !== 'development');

export default {
  // App Details
  appName: 'Starter Pack',

  // Build Configuration - eg. Debug or Release?
  DEV: devMode,

  // Google Analytics - uses a 'dev' account while we're testing
// enable below
  //gaTrackingId: (devMode) ? 'UA-XXXXXXXX-X' : 'UA-XXXXXXXX-X',
};