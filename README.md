# A React Starter Kit for Web and Mobile using one codebase
## ** Work in progress **
An app starter kit to create a web app + mobile app, where you don't need to write and maintain two different code bases. 
The project shares the 'business logic' and allows flexibility in View components to ensure your project looks and feels native in each platform.